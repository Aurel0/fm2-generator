import cv2

#input positions (y,x)
up = (224,25)
down = (229,24)
left = (227,22)
right = (226,27)
select = (228,33)
start = (228,39)
b = (228,46)
a = (228,53)

#threshold value between 0 and 255
limit = 150

run_name = 'run'
video = cv2.VideoCapture(run_name + ".mp4")

emulator_rate = 39375000 / 655171
video_fps = video.get(cv2.CAP_PROP_FPS)
emulator_frame = 0
video_frame = -1

f = open(run_name + ".fm2", "w")
lines= ['version 3',
        'emuVersion 22020', 
        'rerecordCount 0', 
        'palFlag 0', 
        'romFilename rom', 
        'romChecksum base64:0000000000000/00000000==', 
        'guid 00000000-0000-0000-0000-000000000000', 
        'fourscore 0', 
        'microphone 0',
        'port0 1',
        'port1 0',
        'port2 0',
        'FDS 0',
        'NewPPU 0',
        'comment offset 0']

for line in lines:
    f.write(line+"\n")

while(True):
    while round(emulator_frame*video_fps/emulator_rate)> video_frame:
        video_frame += 1
        #reading from frame
        ret,frame = video.read()
        if not ret: break
    
    line = list("|0|........|||") #line without any inputs

    if ret: #if the video continues
            
    	#note that the colors are in BGR format
    	#in my case the button is either black or white check the value of one component is sufficient
        if frame[right][0] > limit: 
    	    line[3] = 'R'
        if frame[left][0] > limit:
            line[4] = 'L'
        if frame[down][0] > limit:
            line[5] = 'D'
        if frame[up][0] > limit:
            line[6] = 'U'
        if frame[start][0] > limit:
            line[7] = 'T'
        if frame[select][0] > limit:
            line[8] = 'S'
        if frame[b][0] > limit:
            line[9] = 'B'
        if frame[a][0] > limit:
            line[10] = 'A'
    else:
        break
    
    f.write("".join(line)+"\n")
    emulator_frame += 1

video.release()
cv2.destroyAllWindows()